# Module_1-Example_Project

This is a project showcasing the current knowledge with HTML, CSS, SASS, JavaScript, jQuery, Select2 and Flatpickr.

The SASS folder is left intentionally for viewing purposes.

## I. Known issues
**1.** Main page  
~~1. Distances with % - how does it relate to other items, nested items and how to manipulate it~~  
~~2. Labels and placeholders appear the same~~  
~~3. Centering the info button and the label while keeping all distances and sizes the same + make the button bigger~~  
~~4. Review of the used checkbox styling method~~  
~~5. Button and text next to it not acting as expected - line spacing -1~~  
~~6. Fix all form input names and data~~  
~~7. Add functionality with JavaScript and an example submit console log~~  
~~8. Picture in the design has darker elements~~  
~~9. Add disabled button class until all data is entered correctly~~  
~~10. Font doesn't apply to Explorer~~  
~~11. Fix tooltip to show only on the icon of password~~  
~~12. Add password icon swap, stack and view password functionality~~  
~~13. Design question - shouldn't the tooltip arrow come out of the icon?~~  
~~14. Review the custom select~~  
~~15. Dropdown label doesn't focus dropdown on click - DO NOT DO~~  
~~16. SASS - Extend vs mixin~~  
~~17. Error showcase for the date picker and custom select~~  
~~18. Date picker - date on hover has different colors for current day, selected day and other days~~  
~~19. Password error margin is less than other fields because of parent size~~  
20. Explorer - on checkbox hover doesn't change color to gray, on label it does on field reenter  
~~21. Explorer - License field small text is buggy when changing to error text~~  
~~22. Explorer - autocomplete off doesn't work~~  
