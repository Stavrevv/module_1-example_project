// Functions
function submitBtnDisabler() {
    let checkForErrors = $('p').filter('.error-text-visibile').length !== 0;
    let checkForMissingInputs = $('input').filter(function () { return this.value.length <= 0; }).length !== 0;
    let checkCheckBoxes = $('input:checkbox:checked').length !== 2;
    let selectChecker = $('#honorificDropdown').select2('data')[0].text.length <= 0;
    let dateCheker = false;

    // If any error checks are true, disable button
    if (checkForErrors || checkForMissingInputs || checkCheckBoxes || selectChecker || dateCheker) {
        $('#formSubmitBtn').addClass('buttonDisabled');
        return;
    }

    $('#formSubmitBtn').removeClass('buttonDisabled');
};

function setSuccessScreen() {
    // Define main container
    let mainContainer = $('#userInputField');
    let containerHeight = $('#userInputField').height();
    let windowWidth = $(document).width();

    if (windowWidth <= 1024) {
        containerHeight = 574;
    }

    if (windowWidth < 750) {
        containerHeight = 474;
    }

    // Clear old elements
    mainContainer[0].innerHTML = '';
    mainContainer.css('min-height', containerHeight);

    // Create the elements
    let image = $('<img class="success-image" src="assets/images/success.svg" alt="">');
    let title = $('<h1></h1>').text('נרשמת בהצלחה!');
    let message = $('<p></p>').text('שלחנו לך מייל עם כל פרטי ההרשמה שלך לכתובת moshe@gmail.com על מנת לוודא שזה אתה.');

    // Add the elements
    mainContainer.append(image, title, message);
    mainContainer.addClass('userInputField');
};

function errorDisplay(element, elementValue, regEx) {
    let nextElement = element.next(); // the error field or the small text field
    let nextNextElement = nextElement.next(); // the error field after a small field

    // check if the next element is a small field
    if (nextElement.hasClass('small-text')) {
        if (!regEx.test(elementValue)) {
            element.addClass('error-border');
            nextElement.css('display', 'none');
            nextNextElement.removeClass('error-display-none');
            nextNextElement.addClass('error-text-visibile');
            return;
        }

        element.removeClass('error-border');
        nextElement.css('display', 'inline-block');
        nextNextElement.addClass('error-display-none');
        nextNextElement.removeClass('error-text-visibile');
        return;
    }

    if (!regEx.test(elementValue)) {
        element.addClass('error-border');
        nextElement.addClass('error-text-visibile');
        return;
    }

    element.removeClass('error-border');
    nextElement.removeClass('error-text-visibile');
};

function errorDisplayPassword(password) {
    let visibilityBtn = password.prev();
    visibilityBtn.css('left', '30px');

    if (password[0].type === 'password') {
        password.addClass('error-password-hide');
        password.next().addClass('error-text-visibile');
        return;
    }

    password.addClass('error-password-show');
    password.next().addClass('error-text-visibile');
};

function errorRemovePassword(password) {
    let visibilityBtn = password.prev();
    visibilityBtn.css('left', '0');

    if (password[0].type === 'password') {
        password.removeClass('error-password-hide');
        password.next().removeClass('error-text-visibile');
        return;
    }

    password.removeClass('error-password-show');
    password.next().removeClass('error-text-visibile');
};

function errorDisplaySelect() {
    let element = $('.select2');
    let nextElement = element.next();
    let innerElement = $('.select2-selection');

    if ($('#honorificDropdown').select2('data')[0].text.length <= 0) {
        errorSetNoIcon(innerElement, nextElement);
        return;
    }

    errorRemoveNoIcon(innerElement, nextElement);
};

function errorDisplayDatePicker() {
    let element = $('#birthDateContainer').find('input');
    let elementValue = element.val();
    let nextElement = element.parent().next(); // get the parent of the input and the next element after it
    let regEx = /\w+/;

    if (!regEx.test(elementValue)) {
        errorSetNoIcon(element, nextElement);
        return;
    }

    errorRemoveNoIcon(element, nextElement);
};

function errorSetNoIcon(elementForBorder, elementForText) {
    elementForBorder.addClass('error-border-no-icon');
    elementForText.addClass('error-text-visibile');
};

function errorRemoveNoIcon(elementForBorder, elementForText) {
    elementForBorder.removeClass('error-border-no-icon');
    elementForText.removeClass('error-text-visibile');
};

// Event listeners set on document load and required values
$(document).ready(function () {
    // Focus with JS, because CSS :focus is not supported in explorer
    $('input').focusin(function () {
        $(this).addClass('borderFocus');
    });

    $('input').focusout(function () {
        $(this).removeClass('borderFocus');
    });

    // Validation - similar imputs
    $('input').keyup(function () {
        let element = $(this);
        let elementName = element[0].name;
        let elementValue = element.val();

        let regEx;

        switch (elementName) {
            // First name and Last name
            case 'fname':
            case 'lname':
                regEx = /^[A-Za-z]+$/;
                break;

            // E-mail
            case 'email':
                regEx = /^\w+[\.\w]*@\w+\.\w+$/;
                break;

            // License number
            case 'lnumber':
                regEx = /^\d{10}$/;
                break;

            // Phone Number
            case 'phone':
                regEx = /^\d+$/;
                break;

            default:
                return;
        }

        // Send the element and the result to be handled by the error display function
        errorDisplay(element, elementValue, regEx);
    });

    // Password and Repassword
    $('#password, #repassword').keyup(function () {
        // Get elements and values
        let passwordElement = $('#password');
        let passwordValue = passwordElement.val();
        let rePasswordElement = $('#repassword');
        let rePasswordValue = rePasswordElement.val();

        let passwordBtn = passwordElement.prev();
        let rePasswordBtn = rePasswordElement.prev();

        passwordBtn.css('visibility', 'visible');
        rePasswordBtn.css('visibility', 'visible');

        // Set regex and test for it
        let testResult = passwordValue === rePasswordValue;

        if (passwordValue.length <= 1) {
            testResult = false;
        }

        // Set errors if a test didn't pass
        if (!testResult) {
            errorDisplayPassword(passwordElement);
            errorDisplayPassword(rePasswordElement);
            return;
        }

        // Remove errors if tests passed
        errorRemovePassword(passwordElement);
        errorRemovePassword(rePasswordElement);
    });

    // Display password visibility icons
    $('input[type="password"]').keyup(function () {
        let element = $(this);
        let elementType = element[0].type;

        if (elementType === 'password') {
            element.addClass('password-hide');
            element.removeClass('password-show');
            return;
        }

        element.addClass('password-show');
        element.removeClass('password-hide');
    });

    // On click of the visibility icons
    $('.passVisibilityButton').click(function () {
        let inputField = $(this).next()[0];
        let currentType = inputField.type;

        if (currentType === 'password') {
            inputField.type = 'text';

            if (inputField.classList.contains('error-password-hide')) {
                inputField.classList.remove('error-password-hide');
                inputField.classList.add('error-password-show');
                // return;
            }

            inputField.classList.remove('password-hide');
            inputField.classList.add('password-show');
            return;
        }

        inputField.type = 'password';

        if (inputField.classList.contains('error-password-show')) {
            inputField.classList.add('error-password-hide');
            inputField.classList.remove('error-password-show');
            // return;
        }

        inputField.classList.add('password-hide');
        inputField.classList.remove('password-show');
    });

    // Dropdown - config
    $('#honorificDropdown').select2({
        placeholder: 'Honorific form',
        width: '100%',
        minimumResultsForSearch: 'Infinity',
    });

    // Dropdown - error display
    $('#honorificDropdown').on('select2:select', submitBtnDisabler); // check if there is a value to change the submit button
    $('.select2').click(errorDisplaySelect); // set error on click if no value is selected
    $('#honorificDropdown').on('select2:select', errorDisplaySelect); // remove error on value set

    // Date picker
    flatpickr('#birthDateContainer'); // Transforms the container
    flatpickr.localize(flatpickr.l10ns.he); // Sets the language
    flatpickr.l10ns.default.firstDayOfWeek = 7; // Sets the day of the week to be shown first

    // Date picker - config
    $('#birthDateContainer').flatpickr({
        wrap: true, // used to wrap a component, in this case to have a clear and calendar button
        dateFormat: 'd/m/Y', // sets the date format
        maxDate: new Date(), // sets the maximum selectable date - in this case the current date
        monthSelectorType: 'static', // removes the month select option
        disableMobile: 'true', // removes the built in automatic mobile responsiveness
        onChange: function() {
            errorDisplayDatePicker();
            submitBtnDisabler();
        }
    });

    // Date picker - error handling
    $('#birthDateContainer').keyup(errorDisplayDatePicker);

    // On page load, check window size and change the placeholders if needed of the date picker and custom select
    if ($(document).width() < 400) {
        // Date picker
        let datePicker = $('#birthDateContainer input');
        datePicker.attr('placeholder', '...Select');

        // Custom select
        $('#honorificDropdown').select2({
            placeholder: 'Honorific...',
            width: '100%',
            minimumResultsForSearch: 'Infinity',
        });
    }

    // Button Register - grey and unclickable until all fields are valid
    // Visibility
    $('input').keyup(submitBtnDisabler);
    $('input:checkbox').change(submitBtnDisabler);

    // Print results to console
    $('#formSubmitBtn').click(function () {
        // If the button is in error state - stop this function
        if ($('#formSubmitBtn').hasClass('buttonDisabled')) {
            return;
        }

        // Get form data
        let data = $('form').serializeArray();

        // Transform data to an object with key-data pairs
        let dataFinal = {};

        // Traditional loop because internet explorer is awesome
        for (let i = 0; i < data.length; i++) {
            dataFinal[data[i].name] = data[i].value;
        }

        // Send to server
        console.log('The submitted data is: \n', dataFinal);
        setSuccessScreen();
    });
});
